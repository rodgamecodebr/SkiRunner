﻿
/*---------------------------------------------------------------------------------------
 *
 * SkiRunner
 * 
 * GameInput
 * It handles control key/button presses and gestures
 * 
 * Rodrigo dos Reis
 * 
 * ---------------------------------------------------------------------------------------
 * */

using System;
using UnityEngine;

namespace SkiRunner.GameInput
{
    public class PlayerInput : MonoBehaviour
    {
        // type of game input
        public enum InputType
        {
            Keyboard,
            Touch
        }

        public InputType inputType;

        // hold initial and final touch positions to read swipes
        public Vector3 clickPosition;
        public Vector3 currentPosition;

        // type of swipe generated after reading input
        public bool pressedRight;
        public bool pressedLeft;
        public bool swipeUp;

        // can read input from player?
        public bool canReadInput;

        // triggers event after a swipe has been detected
        public event Action OnSwipeDetected;


        // Update is called once per frame
        void Update()
        {
            Debug.Log("Read Input: " + canReadInput);

            // Start reading inputs when playing state will be on
            if (canReadInput)
            {
                if (inputType == InputType.Keyboard)
                    Controls();
                else if (inputType == InputType.Touch)
                    SwipeGestures();
            }
        }


        //---------------------------
        // Write something here.
        //---------------------------

        public void StartGettingInput()
        {
            canReadInput = true;
        }

        //---------------------------
        // Write something here.
        //---------------------------

        void Controls()
        {
            if (Input.GetKeyDown(KeyCode.LeftArrow))
                pressedLeft = true;
            if (Input.GetKeyUp(KeyCode.LeftArrow))
                pressedLeft = false;
            if (Input.GetKeyDown(KeyCode.RightArrow))
                pressedRight = true;
            if (Input.GetKeyUp(KeyCode.RightArrow))
                pressedRight = false;
        }


        //---------------------------
        // Write something here.
        //---------------------------

        void SwipeGestures()
        {
            bool getFirstTouch = false;
            bool getSwipeGesture = false;

            if (!getFirstTouch)
            {
                if (Input.GetMouseButtonDown(0))
                    clickPosition = Input.mousePosition;

                getFirstTouch = true;
                getSwipeGesture = true;
            }

            if (getSwipeGesture)
            {
                if (Input.GetMouseButton(0))
                    currentPosition = Input.mousePosition;

                // subtract the initial and final vector to detect the swipe type
                Vector3 resultVector = clickPosition - currentPosition;

                // if one of the values is less than zero, make it positive for comparison
                if (resultVector.x < 0)
                    resultVector.x *= -1;
                if (resultVector.y < 0)
                    resultVector.y *= -1;

                // if result vector x axis is greater than y axis, it is a side swipe
                if(resultVector.x > resultVector.y)
                {
                    // Swipe to the right
                    if (clickPosition.x < currentPosition.x)
                        pressedRight = true;

                    // Swipe to the left
                    if (clickPosition.x > currentPosition.x)
                        pressedLeft = true;

                    swipeUp = false;
                }
                else if (resultVector.x <= resultVector.y)
                {
                    swipeUp = true;

                    pressedLeft = false;
                    pressedRight = false;
                }

                if (Input.GetMouseButtonUp(0))
                {
                    getSwipeGesture = false;
                    canReadInput = false;

                    // triggers event after which swipe type was detected
                    if (OnSwipeDetected != null)
                        OnSwipeDetected();

                    clickPosition = Vector3.zero;
                    currentPosition = Vector3.zero;

                    swipeUp = false;
                }
            }
        }
    }
}


