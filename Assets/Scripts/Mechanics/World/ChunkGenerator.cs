﻿
/*---------------------------------------------------------------------------------------
 *
 * SkiRunner
 * 
 * World
 * It handles environment locomotion and instantiation
 * 
 * Rodrigo dos Reis
 * 
 * ---------------------------------------------------------------------------------------
 * */

using SkiRunner.Core;
using UnityEngine;

namespace SkiRunner.Game
{
    public class ChunkGenerator : Singleton<ChunkGenerator>
    {
        // array of chunk models
        public Chunk[] worldChunks;

        // chunk that is going to be generated
        public Chunk nextChunk;

        // gameobject to be instantiated
        public Chunk chunkModel;

        public int chunkIndex = -1;

        // parent transform that moves as long as the player is alive (chunk movable)
        public Transform worldChunksCreated;

        // spawn point where the new chunks will be generated
        public Transform chunkSpawn;

        // Use this for initialization
        void Start()
        {
            
        }

        // When detected on trigger creates the chunk
        void OnTriggerEnter(Collider col)
        {
            if (col.transform.tag.Equals("Generator"))
            {
                chunkIndex++;

                // Build different chunks according to index and 
                nextChunk = Instantiate(worldChunks[chunkIndex], chunkSpawn.transform.position, chunkModel.transform.rotation) as Chunk;

                // this chunk is not a model so it can move
                nextChunk.isChunkModel = false;

                // add it as child of moving world
                nextChunk.transform.SetParent(worldChunksCreated);

                // make the new chunk spawn as the spawn object
                chunkSpawn = nextChunk.nextSpawn;

                // set chunk index to restart again after going through all chunk models
                if (chunkIndex == worldChunks.Length - 1)
                    chunkIndex = -1;
            }
        }
    }
}