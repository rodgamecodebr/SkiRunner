﻿
/*---------------------------------------------------------------------------------------
 *
 * SkiRunner
 * 
 * Eraser 
 * It handles chunk deletion of the scene
 * 
 * Rodrigo dos Reis
 * 
 * ---------------------------------------------------------------------------------------
 * */

using UnityEngine;

namespace SkiRunner.Mechanics.World
{
    public class Eraser : MonoBehaviour
    {
        // When detected on trigger deletes the chunk
        void OnTriggerEnter(Collider col)
        {
            if (col.transform.tag.Equals("Delete"))
            {
                Destroy(col.transform.parent.parent.gameObject);
            }
        }
    }
}


