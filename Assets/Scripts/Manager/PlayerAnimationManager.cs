﻿
/*---------------------------------------------------------------------------------------
 *
 * SkiRunner
 * 
 * PlayerAnimationManager
 * It handles player animation states
 * 
 * Rodrigo dos Reis
 * 
 * ---------------------------------------------------------------------------------------
 * */

using SkiRunner.Core;
using SkiRunning.Base;
using SkiRunner.Common;

namespace SkiRunner.Manager
{
    public class PlayerAnimationManager : Singleton<PlayerAnimationManager>
    {
        public float currentAnimationLength;

        StartAnimationState startAnimation;
        SkiAnimationState skiAnimation;

        public GameAnimationState currentGameAnimationState;

        public void SetPlayerAnimationState(CommonTypes.CurrentPlayerAnimation state)
        {
            if (currentGameAnimationState != null)
                Destroy(currentGameAnimationState);

            switch(state)
            {
                case CommonTypes.CurrentPlayerAnimation.Start:
                    
                    startAnimation = gameObject.AddComponent<StartAnimationState>();
                    currentGameAnimationState = startAnimation;

                    currentAnimationLength = startAnimation.animationLength;

                    break;

                case CommonTypes.CurrentPlayerAnimation.Skiing:

                    skiAnimation = gameObject.AddComponent<SkiAnimationState>();
                    currentGameAnimationState = skiAnimation;

                    break;
            }
        }
    }
}


