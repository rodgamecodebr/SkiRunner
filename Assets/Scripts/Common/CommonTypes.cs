﻿
using UnityEngine;

namespace SkiRunner.Common
{
    public static class CommonTypes 
    {
        public enum CurrentGameplayState
        {
            None,
            Intro,
            Playing,
            CalculatePoints,
            GameOver
        }

        public enum CurrentPlayerAnimation
        {
            Start,
            Skiing,
            RightSki,
            LeftSki,
            Jump,
            Dead
        }
    }
}


