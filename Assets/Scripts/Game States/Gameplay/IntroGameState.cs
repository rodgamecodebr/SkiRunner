﻿
/*---------------------------------------------------------------------------------------
 *
 * SkiRunner
 * 
 * IntroGameState
 * It handles gameplay intro initializations and setup
 * 
 * Rodrigo dos Reis
 * 
 * ---------------------------------------------------------------------------------------
 * */

using SkiRunner.Base;
using SkiRunner.Common;
using SkiRunner.Game;
using SkiRunner.Manager;
using SkiRunning.Mechanics.CameraMovement;
using UnityEngine;
using UnityEngine.UI;

namespace SkiRunner.GameStates.Gameplay
{
    public class IntroGameState : GameplayState
    {
        public Transform mainCamera;

        public Button startBtn;

        private void Start()
        {
            Init();
        }


        //---------------------------
        // Init Intro Game State
        //---------------------------

        public override void Init()
        {
            base.Init();

            LoadResources();

            CameraMove.instance.startMove = true;

            Player.instance.StartSkiing();
        }


        //---------------------------
        // Clean resources 
        //---------------------------

        public override void Clean()
        {
            base.Clean();

            Destroy(this);
        }


        //------------------------------
        // Set all required resources
        //------------------------------

        void LoadResources()
        {
            mainCamera = GameManager.Instance.mainCamera;
            startBtn = GameManager.Instance.startButton;
        }
    }
}


