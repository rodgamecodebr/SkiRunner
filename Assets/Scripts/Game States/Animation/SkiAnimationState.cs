﻿
/*---------------------------------------------------------------------------------------
 *
 * SkiRunner
 * 
 * SkiAnimationState
 * It handles player mechanics and game values
 * 
 * Rodrigo dos Reis
 * 
 * ---------------------------------------------------------------------------------------
 * */

using SkiRunning.Base;
using UnityEngine;

public class SkiAnimationState : GameAnimationState
{
	public float animationLength;

	public string paramName = "Ski";

	private void Start()
	{
		Ski();
	}

	public void Ski()
	{
		ChangeParameters(paramName, true);

		if (animator.GetCurrentAnimatorStateInfo(0).IsName("Ski"))
			animationLength = animator.GetCurrentAnimatorStateInfo(0).length;

		Debug.Log(animationLength);
	}
}
