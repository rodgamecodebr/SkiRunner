﻿
/*---------------------------------------------------------------------------------------
 *
 * SkiRunner
 * 
 * Eraser 
 * It handles chunk deletion of the scene
 * 
 * Rodrigo dos Reis
 * 
 * ---------------------------------------------------------------------------------------
 * */

using SkiRunner.Game;
using UnityEngine;

public class Coin : MonoBehaviour
{
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        transform.Rotate(Vector3.forward * 20f * Time.deltaTime);
    }

    private void OnTriggerEnter(Collider col)
    {
        if (col.tag.Equals("Player"))
        {
            Player.instance.playerPoints.coins += 1;

            Destroy(this.gameObject);

            if (Player.instance.playerPoints.coins % 20 == 0)
                Player.instance.playerLocomotion.playerSpeed += 1;
        }
    }
}
