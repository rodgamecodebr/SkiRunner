﻿
/*---------------------------------------------------------------------------------------
 *
 * SkiRunner
 * 
 * Chunk 
 * It Handles game environment chunk that has items or obstacles and
 * gets generated when playing the game.
 * 
 * Rodrigo dos Reis
 * 
 * ---------------------------------------------------------------------------------------
 * */

using UnityEngine;

namespace SkiRunner.Game
{
    public class Chunk : MonoBehaviour
    {
        public Transform nextSpawn;

        public bool isChunkModel;

        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            if (Player.instance.playerLocomotion.canLocomote && !isChunkModel)
                transform.Translate(Vector3.back * Player.instance.playerLocomotion.playerSpeed * Time.deltaTime);
        }
    }
}


